package com.feityz.handler.shef;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.model.variable.Variable;
import com.bstek.uflo.process.handler.AssignmentHandler;
import com.bstek.uflo.process.node.TaskNode;
import com.feityz.common.BaseEntity;
import com.feityz.system.entity.Dept;
import com.feityz.system.entity.User;
import com.feityz.system.service.IDeptService;
import com.feityz.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 隐患辖区主管
 */
@Component("areaManager")
@ConditionalOnProperty(name = "system", havingValue = "yz")
public class AreaManagerHandler implements AssignmentHandler {

    @Autowired
    private IUserService userService;
    @Autowired
    private IDeptService deptService;

    @Override
    public Collection<String> handle(TaskNode taskNode, ProcessInstance processInstance, Context context) {

        //List<Variable> variables = context.getProcessService().createProcessVariableQuery().processInstanceId(processInstance.getId()).key("dept").list();

        List<Variable> variables = context.getProcessService().createProcessVariableQuery().processInstanceId(processInstance.getId()).key("deptManager").list();

        Assert.isTrue(CollectionUtil.isNotEmpty(variables),"辖区主管变量为空");

        String deptManagers = variables.get(0).getValue().toString();

        String[] deptManager = deptManagers.split(",");


        /*String deptsStr = variables.get(0).getValue().toString();

        //获取流程中的部门,部门可能是个数组
        String[] depts = deptsStr.split(",");
        List<Dept> deptLst = new ArrayList<>();
        for (String deptName:depts) {
            Dept dept = deptService.lambdaQuery().eq(Dept::getDeptName,deptName).one();
            Assert.notNull(dept,"流程引擎找不到名称为"+deptName+"的部门");
            deptLst.add(dept);
        }

        List<User> users = userService.lambdaQuery()
                .in(User::getDept, deptLst.stream().map(BaseEntity::getId).collect(Collectors.toList()))
                .inSql(User::getId,"select user_id from re_user_role where role_id in " +
                        "(select id from sys_role " +
                        "where role_name='辖区主管')")
                .list();

        List<String> result = users.stream().map(e -> e.getId().toString()).collect(Collectors.toList());
        Assert.notEmpty(result, Arrays.toString(depts) + "部门下没有辖区主管,请检查人员配置");*/

        List<User> users = userService.lambdaQuery().in(User::getUserNum,deptManager).list();

        List<String> result = users.stream().map(e -> e.getId().toString()).collect(Collectors.toList());

        return result;

    }

    @Override
    public String desc() {
        return "隐患辖区主管";
    }
}
