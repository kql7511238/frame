package com.feityz.handler.shef;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.model.variable.Variable;
import com.bstek.uflo.process.handler.AssignmentHandler;
import com.bstek.uflo.process.node.TaskNode;
import com.feityz.system.entity.User;
import com.feityz.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component("zgqrUserHandler")
@ConditionalOnProperty(name = "system", havingValue = "yz")
public class ZgqrUserHandler implements AssignmentHandler {

    @Autowired
    private IUserService userService;

    @Override
    public Collection<String> handle(TaskNode taskNode, ProcessInstance processInstance, Context context) {
        List result = new ArrayList();

        List<Variable> variables = context.getProcessService().createProcessVariableQuery().processInstanceId(processInstance.getId()).key("zgqrUsers").list();

        Assert.isTrue(CollectionUtil.isNotEmpty(variables),"整改确认人员为空");

        String zgqrUsers = variables.get(0).getValue().toString();

        String userNums [] = zgqrUsers.split(",");

        List<User> users = userService.lambdaQuery().in(User::getUserNum, userNums).list();

        result = users.stream().map(e->e.getId().toString()).collect(Collectors.toList());

        Assert.notEmpty(result, "整改确认人员为空,请检查人员配置");

        return result;
    }

    @Override
    public String desc() {
        return "隐患/整改确认人员";
    }
}
