package com.feityz.handler.shef;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.model.variable.Variable;
import com.bstek.uflo.process.handler.AssignmentHandler;
import com.bstek.uflo.process.node.TaskNode;
import com.feityz.system.entity.User;
import com.feityz.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component("kdManager")
@ConditionalOnProperty(name = "system", havingValue = "yz")
public class KdManager implements AssignmentHandler {

    @Autowired
    private IUserService userService;

    @Override
    public Collection<String> handle(TaskNode taskNode, ProcessInstance processInstance, Context context) {
        List<Variable> variables = context.getProcessService().createProcessVariableQuery().processInstanceId(processInstance.getId()).key("kdzg").list();

        Assert.isTrue(CollectionUtil.isNotEmpty(variables),"开单人员主管为空");

        String deptManagers = variables.get(0).getValue().toString();

        String[] deptManager = deptManagers.split(",");

        List<User> users = userService.lambdaQuery().in(User::getUserNum,deptManager).list();

        List<String> result = users.stream().map(e -> e.getId().toString()).collect(Collectors.toList());

        return result;

    }

    @Override
    public String desc() {
        return "开单人员主管";
    }
}
