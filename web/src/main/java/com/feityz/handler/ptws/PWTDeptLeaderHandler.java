package com.feityz.handler.ptws;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.model.variable.Variable;
import com.bstek.uflo.process.handler.AssignmentHandler;
import com.bstek.uflo.process.node.TaskNode;
import com.feityz.common.BaseEntity;
import com.feityz.system.entity.Dept;
import com.feityz.system.entity.User;
import com.feityz.system.service.IDeptService;
import com.feityz.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component("ptwDeptLeader")
@ConditionalOnProperty(name = "system", havingValue = "yz")
public class PWTDeptLeaderHandler implements AssignmentHandler {

    @Autowired
    private IDeptService deptService;
    @Autowired
    private IUserService userService;

    @Override
    public String desc() {
        return "ptws/部门主管";
    }

    @Override
    public Collection<String> handle(TaskNode taskNode, ProcessInstance processInstance, Context context) {

        List<Variable> variables = context
                .getProcessService()
                .createProcessVariableQuery()
                .processInstanceId(processInstance.getId()).key("dept").list();

        Assert.isTrue(CollectionUtil.isNotEmpty(variables),"部门变量为空");

        String deptsStr = variables.get(0).getValue().toString();

        String[] depts = deptsStr.split(",");

        List<Dept> deptLst = new ArrayList<>();
        for (String deptNum:depts) {
            Dept dept = deptService.lambdaQuery().eq(Dept::getDeptNum,deptNum).one();
            Assert.notNull(dept,"流程引擎找不到编号为"+deptNum+"的部门");
            deptLst.add(dept);
        }

        List<User> users = userService.lambdaQuery()
                .in(User::getDept, deptLst.stream().map(BaseEntity::getId).collect(Collectors.toList()))
                .inSql(User::getId,"select user_id from re_user_role where role_id in " +
                        "(select id from sys_role " +
                        "where role_name='主管')")
                .list();

        List<String> result = users.stream().map(e -> e.getId().toString()).collect(Collectors.toList());

        Assert.notEmpty(result, Arrays.toString(depts) + "部门下没有主管,请检查人员配置");

        return result;
    }
}
