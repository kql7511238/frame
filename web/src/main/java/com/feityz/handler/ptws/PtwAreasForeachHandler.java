package com.feityz.handler.ptws;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.lang.Assert;
import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.model.variable.Variable;
import com.bstek.uflo.process.handler.ForeachHandler;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public class PtwAreasForeachHandler implements ForeachHandler {
    @Override
    public String desc() {
        return "ptw会签";
    }

    @Override
    public Collection<Object> handle(ProcessInstance processInstance, Context context) {
        List<Variable> variables = context.getProcessService().createProcessVariableQuery().processInstanceId(processInstance.getRootId()).key("AREAJointly").list();

        Assert.isTrue(CollectionUtil.isNotEmpty(variables),"辖区变量为空");

        String areaStr = variables.get(0).getValue().toString();

        return ListUtil.toList(areaStr.split(","));
    }
}
