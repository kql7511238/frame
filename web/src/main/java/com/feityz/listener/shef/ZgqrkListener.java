package com.feityz.listener.shef;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.model.task.Task;
import com.bstek.uflo.model.variable.Variable;
import com.bstek.uflo.process.listener.TaskListener;
import com.bstek.uflo.process.node.TaskNode;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ZgqrkListener implements TaskListener {
    @Override
    public boolean beforeTaskCreate(Context context, ProcessInstance processInstance, TaskNode node) {
        return false;
    }

    @Override
    public void onTaskCreate(Context context, Task task) {


    }

    @Override
    public void onTaskComplete(Context context, Task task) {
        List<Variable> agrees = context.getProcessService().createProcessVariableQuery().processInstanceId(task.getProcessInstanceId()).key("agree").list();

        Assert.isTrue(CollectionUtil.isNotEmpty(agrees),"请求变量为空");

        String agree = agrees.get(0).getValue().toString();

        if("3".equals(agree)) {
            Map variable = new HashMap();
            variable.put("zgqr", "1");
            context.getTaskService().setTaskVariable(variable, task.getId());
        }
    }

    @Override
    public String desc() {
        return "整改确认";
    }
}
