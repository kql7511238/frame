package com.feityz.provider;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.assign.AssigneeProvider;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import com.feityz.system.entity.User;
import com.feityz.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class ApplicantProvider implements AssigneeProvider {
    @Autowired
    private IUserService userService;
    @Override
    public boolean isTree() {
        return false;
    }

    @Override
    public String getName() {
        return "按申请人员";
    }

    @Override
    public void queryEntities(PageQuery<Entity> pageQuery, String parentId) {
        Entity entitie = new Entity("1","申请人");
        List<Entity> ls = new ArrayList<>();
        ls.add(entitie);
        pageQuery.setResult(ls);
        pageQuery.setRecordCount(1);
    }

    @Override
    public Collection<String> getUsers(String entityId, Context context, ProcessInstance processInstance) {

        List<String> ls = new ArrayList<>();

        String promoter = processInstance.getPromoter();

        ls.add(promoter);

        return ls;
    }

    @Override
    public boolean disable() {
        return false;
    }
}
