package com.feityz.provider;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.assign.AssigneeProvider;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import com.feityz.system.entity.User;
import com.feityz.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PositionAssigneeProvider implements AssigneeProvider {
    @Autowired
    private IUserService userService;

    @Override
    public boolean isTree() {
        return false;
    }

    @Override
    public String getName() {
        return "按职位";
    }

    @Override
    public void queryEntities(PageQuery<Entity> pageQuery, String parentId) {
        List<Entity> entities = new ArrayList<>();
        IPage<User> page = new Page<>(pageQuery.getPageIndex(), pageQuery.getPageSize());
        page = userService.lambdaQuery()
                .ne(User::getPosition, "")
                .groupBy(User::getPosition)
                .orderByAsc(User::getPosition)
                .select(User::getPosition)
                .page(page);
        List<User> users = page.getRecords();

        users.forEach(n -> {
            Entity en = new Entity(n.getPosition(), n.getPosition());
            entities.add(en);
        });
        pageQuery.setResult(entities);
        pageQuery.setRecordCount((int) page.getTotal());
    }

    @Override
    public Collection<String> getUsers(String entityId, Context context, ProcessInstance processInstance) {
        /*List<String> userIds = userService.lambdaQuery().eq(User::getPosition,entityId).list().stream()
                .map(e->e.getId().toString()).collect(Collectors.toList());*/
        return new ArrayList<>();
    }

    @Override
    public boolean disable() {
        return false;
    }
}
