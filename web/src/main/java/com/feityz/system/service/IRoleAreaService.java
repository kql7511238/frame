package com.feityz.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feityz.system.entity.RoleArea;
import com.feityz.system.input.GetAreaByUserInput;
import com.feityz.system.input.RoleAreaInput;
import com.feityz.system.input.RoleAreaQueryInput;
import com.feityz.system.vo.RoleAreaUserVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IRoleAreaService  extends IService<RoleArea> {
    @Transactional
    void saveRelation(RoleAreaInput input);

    List<RoleAreaUserVo> listAll(RoleAreaQueryInput input);

    void delete(Long id);

    List<String> getAreaByUser(GetAreaByUserInput input);
}
