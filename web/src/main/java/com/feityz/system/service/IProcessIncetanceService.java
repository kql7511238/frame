package com.feityz.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.feityz.system.entity.Dept;
import com.feityz.system.entity.ProcessIncetanceVo;
import com.feityz.system.input.ProcessIncetanceHisInput;
import com.feityz.system.input.ProcessIncetanceInput;
import com.feityz.system.vo.CurrentNodeVo;
import com.feityz.system.vo.NextNodeVo;
import com.feityz.system.vo.RollBackInput;

import java.util.List;
import java.util.Map;

public interface IProcessIncetanceService {
    IPage<ProcessIncetanceVo> pageByAssignee(ProcessIncetanceVo condition, Page<ProcessIncetanceVo> page);

    IPage<ProcessIncetanceVo> pageByPromoter(ProcessIncetanceInput condition, Page<ProcessIncetanceVo> page);

    IPage<ProcessIncetanceVo> pageAll(ProcessIncetanceVo condition, Page<ProcessIncetanceVo> page);

    IPage<ProcessIncetanceVo> pageByPromoterHis(ProcessIncetanceHisInput condition, Page<ProcessIncetanceVo> page);

    void deleteHisInstance(Long instanceId);

    Map getInstanceParms(long instanceId);

    List<CurrentNodeVo> getCurrent(Long bizId);

    void endProcessInstance(RollBackInput input);
}
