package com.feityz.system.service;

import com.bstek.uflo.model.task.Task;
import com.feityz.system.input.ProcessCancleInput;
import com.feityz.system.vo.CancelInput;
import com.feityz.system.vo.NextNodeVo;
import com.feityz.system.vo.ProcessStartInput;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface IProcessRunService {
    //流程启动
    Map start(ProcessStartInput input);

    @Transactional
    void remove(long id);

    @Transactional
    void cancel(CancelInput input);

    Object getVaribale(long instancesId);

    @Transactional
    void cancleProcess(ProcessCancleInput input);
}
