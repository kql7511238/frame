package com.feityz.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feityz.system.entity.UserAndRoles;

public interface IUserAndRoleService extends IService<UserAndRoles> {
}
