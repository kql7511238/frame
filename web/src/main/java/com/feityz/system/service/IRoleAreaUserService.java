package com.feityz.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feityz.system.entity.RoleAreaUser;

public interface IRoleAreaUserService extends IService<RoleAreaUser> {
}
