package com.feityz.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feityz.system.entity.RoleAreaUser;
import com.feityz.system.entity.RoleDeptUser;

public interface IRoleDeptUserService extends IService<RoleDeptUser> {
}
