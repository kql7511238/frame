package com.feityz.system.service.impl;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feityz.system.dao.RoleMapper;
import com.feityz.system.dao.UserAndRolesMapper;
import com.feityz.system.dao.UserMapper;
import com.feityz.system.entity.Dept;
import com.feityz.system.entity.User;
import com.feityz.system.entity.UserAndRoles;
import com.feityz.system.service.IDeptService;
import com.feityz.system.service.IUserService;
import com.feityz.system.vo.UserInput;
import com.feityz.util.HrRequestUtil;
import com.feityz.util.MD5Util;
import com.feityz.util.SpringUtils;
import exception.BizException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserAndRolesMapper userAndRolesMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private IDeptService deptService;

    @Override
    public IPage<User> getPage(User condition, Page<User> page) {
        page.setRecords(userMapper.listPage(page, condition));
        return page;
    }

    @Override
    public List<User> listAll(User condition) {
        QueryWrapper query = new QueryWrapper();
        return userMapper.listPage(condition);
    }

    @Override
    public boolean save(User entity) {
        checkUser(entity);
        String[] roles = entity.getRoles().split(",");
        if (entity.getId() == null) {
            entity.setPassword(MD5Util.getPassword(entity.getUserNum(), entity.getPassword()));
            userMapper.insert(entity);
        } else {
            User sourceUser = userMapper.selectById(entity.getId());
            if (StringUtils.isEmpty(entity.getPassword())) {
                entity.setPassword(sourceUser.getPassword());
            } else {
                entity.setPassword(MD5Util.getPassword(entity.getUserNum(), entity.getPassword()));
            }
            userMapper.updateById(entity);
        }
        // 保存人员角色关系
        QueryWrapper<UserAndRoles> condition = new QueryWrapper<UserAndRoles>();
        condition.eq("user_id", entity.getId());
        userAndRolesMapper.delete(condition);
        for (String roleId : roles) {
            if (StringUtils.isEmpty(roleId)) {
                continue;
            }
            UserAndRoles relation = new UserAndRoles();
            relation.setUserId(entity.getId());
            relation.setRoleId(Long.valueOf(roleId));
            userAndRolesMapper.insert(relation);
        }
        return true;
    }

    @Override
    public boolean saveUserSync(UserInput entity) {
        String userNum = entity.getUserNum();
        User user = lambdaQuery().eq(User::getUserNum,entity.getUserNum()).one();
        if(user==null){
            user = new User();
            user.setUserName(entity.getUserName());
            user.setUserNum(entity.getUserNum());
            user.setPassword(MD5Util.getPassword(entity.getUserNum(), entity.getPassWord()));
            userMapper.insert(user);
        }else{
            //user = new User();
            user.setUserName(entity.getUserName());
            user.setUserNum(entity.getUserNum());
            user.setPassword(MD5Util.getPassword(entity.getUserNum(), entity.getPassWord()));
            userMapper.updateById(user);
        }
        //更新人员部门
        String deptNum = entity.getDeptNum();

        if(StringUtils.isEmpty(deptNum)) return true;

        Dept dept = deptService.lambdaQuery().eq(Dept::getDeptNum,deptNum).one();

        if(dept==null){
            dept = new Dept();
            dept.setDeptNum(deptNum);
            dept.setDeptName(entity.getDeptName());
            deptService.saveOrUpdate(dept);
        }else{
            dept.setDeptName(entity.getDeptName());
            deptService.saveOrUpdate(dept);
        }
        user.setDept(dept.getId());
        userMapper.updateById(user);
        return true;
    }

    @Override
    public User getUserByCode(String code) {
        return this.lambdaQuery().eq(User::getUserNum,code).one();
    }

    @Override
    public void changePassword(User user) {
        User userRes = this.getById(SpringUtils.getLoginUser().getId());
        String oldPwd = userRes.getPassword();
        String passwordInput = MD5Util.getPassword(userRes.getUserNum(), user.getOldPassword());//输入的原密码

        if (!oldPwd.equals(passwordInput)) {
            throw new BizException("原密码不正确");
        } else {
            userRes.setPassword(MD5Util.getPassword(userRes.getUserNum(), user.getPassword()));
            this.updateById(userRes);
        }
    }

    @Override
    public boolean regist(User user) {
        //先查找有没有该用户
        checkUser(user);

        user.setPassword(MD5Util.getPassword(user.getUserNum(), user.getPassword()));

        baseMapper.insert(user);

        return false;
    }

    private boolean checkUser(User user) {
        List<User> users = this.lambdaQuery().eq(User::getUserNum,user.getUserNum()).list();
        //插入时候
        if (user.getId() == null) {
            if (users != null && users.size() > 0) {
                throw new BizException("已存在编号为[" + user.getUserNum() + "]的用户");
            }
        } else {
            //更新操作
            if (users.size() == 1) {
                if (!user.getId().equals(users.get(0).getId())) {
                    throw new BizException("已存在编号为[" + user.getUserNum() + "]的用户");
                }
            }
        }
        return true;
    }

    /**
     * 同步HR人员
     */
    @Override
    //@Scheduled(cron = "*/60 * * * * ?")
    @Transactional
    public void syncHrUser(){
        List<User> users = HrRequestUtil.getHrUser();
        for (User user : users) {
            user.setPassword(MD5Util.getPassword(user.getUserNum(), "123456"));
            Dept dept = deptService.lambdaQuery().eq(Dept::getDeptNum,user.getDeptNum()).one();
            if(dept==null){
                dept = new Dept();
                dept.setDeptNum(user.getDeptNum());
                dept.setDeptName(user.getDeptName());
                deptService.save(dept);
            }
            user.setDept(dept.getId());
            //已经存在的不用插入
            User existUser = lambdaQuery().eq(User::getUserNum,user.getUserNum()).one();

            if(existUser == null){
                baseMapper.insert(user);
            }else{
                //existUser.setPosition(user.getPosition());
                existUser.setDept(dept.getId());
                existUser.setUserName(user.getUserName());
                existUser.setPosition(user.getPosition());
                baseMapper.updateById(existUser);
            }
        }
    }

    /**
     * 同步HR人员
     */
    @Override
    //@Scheduled(cron = "*/60 * * * * ?")
    @Transactional
    public void syncShefUser(){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Map> userData = restTemplate.getForEntity("http://10.17.241.29:8888/api/Admin/UserSys/ListAll", Map.class);

        JSONArray jsonArray = JSONUtil.parseArray(userData.getBody().get("data"));

        List<User> users = jsonArray.toList(User.class);

        for (User user : users) {
            user.setPassword(MD5Util.getPassword(user.getUserNum(), "123456"));
            Long deptId= 0L;
            if(StringUtils.isNotEmpty(user.getDeptNum())) {
                Dept dept = deptService.lambdaQuery().eq(Dept::getDeptNum, user.getDeptNum()).one();
                if (dept == null) {
                    dept = new Dept();
                    dept.setDeptNum(user.getDeptNum());
                    dept.setDeptName(user.getDeptName());
                    dept.setArea(user.getArea());
                    deptService.save(dept);
                }else {
                    dept.setArea(user.getArea());
                    dept.setDeptName(user.getDeptName());
                    deptService.updateById(dept);
                }
                deptId = dept.getId();
            }
            user.setDept(deptId);
            //已经存在的不用插入
            User existUser = lambdaQuery().eq(User::getUserNum,user.getUserNum()).one();

            if(existUser == null){
                baseMapper.insert(user);
            }else{
                //existUser.setPosition(user.getPosition());
                existUser.setDept(deptId);
                existUser.setPosition(user.getPosition());
                existUser.setUserName(user.getUserName());
                existUser.setPosition(user.getPosition());
                baseMapper.updateById(existUser);
            }
        }
    }
}
