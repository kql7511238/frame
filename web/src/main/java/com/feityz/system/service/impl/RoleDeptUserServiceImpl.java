package com.feityz.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feityz.system.dao.RoleAreaUserMapper;
import com.feityz.system.dao.RoleDeptUserMapper;
import com.feityz.system.entity.RoleAreaUser;
import com.feityz.system.entity.RoleDeptUser;
import com.feityz.system.service.IRoleAreaUserService;
import com.feityz.system.service.IRoleDeptUserService;
import org.springframework.stereotype.Service;

@Service
public class RoleDeptUserServiceImpl extends ServiceImpl<RoleDeptUserMapper, RoleDeptUser> implements IRoleDeptUserService {
}
