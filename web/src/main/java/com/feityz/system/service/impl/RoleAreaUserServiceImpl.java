package com.feityz.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feityz.system.dao.RoleAreaUserMapper;
import com.feityz.system.entity.RoleAreaUser;
import com.feityz.system.service.IRoleAreaUserService;
import org.springframework.stereotype.Service;

@Service
public class RoleAreaUserServiceImpl extends ServiceImpl<RoleAreaUserMapper, RoleAreaUser> implements IRoleAreaUserService {
}
