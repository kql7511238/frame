package com.feityz.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feityz.system.dao.RoleAndMenuMapper;
import com.feityz.system.entity.RoleAndMenu;
import com.feityz.system.service.IRoleAndeMenuService;
import org.springframework.stereotype.Service;

/**
 * @author kongqinglin
 */
@Service
public class RoleAndeMenuServiceImpl extends ServiceImpl<RoleAndMenuMapper, RoleAndMenu>
        implements IRoleAndeMenuService {

}
