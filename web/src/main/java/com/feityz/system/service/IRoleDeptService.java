package com.feityz.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feityz.system.entity.RoleArea;
import com.feityz.system.entity.RoleDept;
import com.feityz.system.input.RoleAreaInput;
import com.feityz.system.input.RoleDeptInput;
import com.feityz.system.input.RoleDeptQueryInput;
import com.feityz.system.vo.RoleAreaUserVo;
import com.feityz.system.vo.RoleDeptUserVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IRoleDeptService extends IService<RoleDept> {

    @Transactional
    void saveRelation(RoleDeptInput input);

    List<RoleDeptUserVo> listAll(RoleDeptQueryInput input);

    @Transactional
    void delete(Long id);
}
