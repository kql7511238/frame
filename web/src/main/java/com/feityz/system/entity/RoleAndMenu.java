package com.feityz.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.feityz.common.BaseEntity;
import lombok.Data;

@Data
@TableName("re_role_menu")
public class RoleAndMenu extends BaseEntity {

    private Long roleId;

    private Long menuId;

    public static final String ROLE_ID = "role_id";

    public static final String MENU_ID = "menu_id";

}
