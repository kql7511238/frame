package com.feityz.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.feityz.common.BaseEntity;
import lombok.Data;

@Data
@TableName("re_role_dept")
public class RoleDept extends BaseEntity {

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 辖区名称
     */
    private String deptNum;
}
