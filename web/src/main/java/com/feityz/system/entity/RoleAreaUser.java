package com.feityz.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.feityz.common.BaseEntity;
import lombok.Data;


@Data
@TableName("re_role_area_user")
public class RoleAreaUser extends BaseEntity {
    private Long roleAreaId;

    private Long userId;
}
