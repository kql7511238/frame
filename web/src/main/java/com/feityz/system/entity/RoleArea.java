package com.feityz.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.feityz.common.BaseEntity;
import lombok.Data;

@Data
@TableName("re_role_area")
public class RoleArea extends BaseEntity {

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 辖区名称
     */
    private String area;
}
