package com.feityz.system.entity;

import lombok.Data;

import java.util.Date;

@Data
public class ProcessIncetanceVo {

    private Long id;

    private String subject;

    private String businessId;

    private String currentNode;

    private String currentTask;

    private String promoter;

    //private String assignee;

    private Date createDate;

    private Date endDate;

    private String processName;

    private String processKey;

    private String url;
    //private String promoterName;
    private Long processInstanceId;

}
