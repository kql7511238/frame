package com.feityz.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.feityz.common.BaseEntity;
import lombok.Data;


@Data
@TableName("re_role_dept_user")
public class RoleDeptUser extends BaseEntity {
    private Long roleDeptId;

    private Long userId;
}
