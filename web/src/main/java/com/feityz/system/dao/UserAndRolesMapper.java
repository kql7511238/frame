package com.feityz.system.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feityz.system.entity.UserAndRoles;

public interface UserAndRolesMapper extends BaseMapper<UserAndRoles> {

}
