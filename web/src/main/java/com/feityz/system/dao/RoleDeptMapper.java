package com.feityz.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feityz.system.entity.RoleArea;
import com.feityz.system.entity.RoleDept;

public interface RoleDeptMapper extends BaseMapper<RoleDept> {
}
