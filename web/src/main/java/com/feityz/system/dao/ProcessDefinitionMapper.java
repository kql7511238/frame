package com.feityz.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feityz.system.entity.ProcessDefinitionVo;

public interface ProcessDefinitionMapper extends BaseMapper<ProcessDefinitionVo> {
}
