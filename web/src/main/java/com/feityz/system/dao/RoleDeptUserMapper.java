package com.feityz.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feityz.system.entity.RoleAreaUser;
import com.feityz.system.entity.RoleDept;
import com.feityz.system.entity.RoleDeptUser;

public interface RoleDeptUserMapper extends BaseMapper<RoleDeptUser> {
}
