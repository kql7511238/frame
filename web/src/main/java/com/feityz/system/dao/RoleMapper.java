package com.feityz.system.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feityz.system.entity.Role;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Line
 * @since 2019-11-13
 */
public interface RoleMapper extends BaseMapper<Role> {

}
