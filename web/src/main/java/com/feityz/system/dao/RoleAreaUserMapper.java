package com.feityz.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feityz.system.entity.RoleArea;
import com.feityz.system.entity.RoleAreaUser;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RoleAreaUserMapper extends BaseMapper<RoleAreaUser> {

    @Select("select * from re_role_area where id in ( "
            +"SELECT role_area_id from re_role_area_user where user_id in (select id from sys_user WHERE user_num = #{userNum} "
            +") and role_id in (select id from sys_role where role_num = 'PTWQF'))")
    public List<RoleAreaUser> selectRoleAreaUser(String uerNum);
}
