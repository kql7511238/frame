package com.feityz.system.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feityz.system.entity.RoleAndMenu;

public interface RoleAndMenuMapper extends BaseMapper<RoleAndMenu> {

}
