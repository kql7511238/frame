package com.feityz.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.feityz.system.entity.ProcessIncetanceVo;
import com.feityz.system.entity.TaskInfo;
import com.feityz.system.input.ProcessIncetanceHisInput;
import com.feityz.system.input.ProcessIncetanceInput;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProcessIncetanceMapper {

    List<ProcessIncetanceVo> pageByAssignee(Page<ProcessIncetanceVo> page, @Param("ew") ProcessIncetanceVo condition);

    List<ProcessIncetanceVo> pageByPromoter(Page<ProcessIncetanceVo> page, @Param("ew") ProcessIncetanceInput condition);

    List<ProcessIncetanceVo> pageByPromoterHis(Page<ProcessIncetanceVo> page, @Param("ew") ProcessIncetanceHisInput condition);

    List<ProcessIncetanceVo> pageAll(Page<ProcessIncetanceVo> page, @Param("ew") ProcessIncetanceVo condition);

    void deleteHisInstance(Long instanceId);

    int selectInstanceByBizId(String bizId,long processId);

    int selectHisInstanceByBizId(String bizId,long processId);

    List<Integer> selectHisInstanceIdByBizId(String bizId);

    ProcessIncetanceVo selectCurrentInstance(String bizId);


}
