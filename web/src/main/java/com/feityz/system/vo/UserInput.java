package com.feityz.system.vo;

import lombok.Data;

import java.util.List;

@Data
public class UserInput {

    private String userNum;

    private String userName;

    private String passWord;

    private String deptNum;

    private String deptName;

    //private String roles;
}
