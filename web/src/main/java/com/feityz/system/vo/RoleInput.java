package com.feityz.system.vo;

import lombok.Data;

@Data
public class RoleInput {

    private String roleNum;

    private String roleName;

    private String users;
}
