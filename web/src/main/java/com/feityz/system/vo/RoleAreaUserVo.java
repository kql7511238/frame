package com.feityz.system.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import lombok.Data;

import java.util.List;

@Data
public class RoleAreaUserVo {

    @Excel(name = "人员",orderNum = "3")
    private String userNames;
    @Excel(name = "辖区",orderNum = "2")
    private String area;
    @ExcelIgnore
    private Long id;
    @ExcelIgnore
    private Long roleId;
    @Excel(name = "角色",orderNum = "1")
    private String roleName;
    @ExcelIgnore
    private List<Long> userId;
}
