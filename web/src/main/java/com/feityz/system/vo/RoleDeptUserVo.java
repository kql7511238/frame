package com.feityz.system.vo;

import lombok.Data;

import java.util.List;

@Data
public class RoleDeptUserVo {

    private String userNames;
    private String dept;
    private String deptName;
    private Long id;
    private Long roleId;
    private String roleName;
    private List<Long> userId;
}
