package com.feityz.system.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author kongqinglin
 */
@Data
public class UserTaskInput {

    private String subject;

    private String starttime;

    private String endtime;

    private String userNum;

    private String processKey;

    @JsonIgnore
    private List<String> processKeys;

    private String taskType;

    private int page;

    private int limit;

    @JsonIgnore
    private List<String> assignees;

    private String taskName;

    private String url;
}
