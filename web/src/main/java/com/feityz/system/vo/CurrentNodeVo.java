package com.feityz.system.vo;

import com.bstek.uflo.process.node.UserData;
import com.feityz.system.entity.User;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 下一节点的处理人员和节点名称
 */
@Data
public class CurrentNodeVo {

    private String nodeName;

    private List<User> assignee = new ArrayList<>();

    private String url;

    private long instanceId;

    private long taksId;

    private List<UserData> userData = new ArrayList<>();
}
