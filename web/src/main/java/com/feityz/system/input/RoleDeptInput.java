package com.feityz.system.input;

import lombok.Data;

import java.util.List;

@Data
public class RoleDeptInput {

    private Long id;

    private String dept;

    private Long roleId;

    private List<Long> userId;
}
