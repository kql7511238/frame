package com.feityz.system.input;

import lombok.Data;

import java.util.List;

@Data
public class RoleAreaInput {

    private Long id;

    private String area;

    private Long roleId;

    private List<Long> userId;
}
