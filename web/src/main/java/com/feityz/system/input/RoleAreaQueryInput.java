package com.feityz.system.input;

import lombok.Data;

import java.util.List;

@Data
public class RoleAreaQueryInput {
    private String erea;

    private List<String> roleName;
}
