package com.feityz.system.input;

import com.feityz.system.entity.ProcessIncetanceVo;
import lombok.Data;

@Data
public class IncetanceInput extends ProcessIncetanceVo {

    private int page;

    private int limit;

    private String processKey;
}
