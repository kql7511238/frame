package com.feityz.system.input;

import lombok.Data;

@Data
public class ProcessIncetanceInput {
    private int limit;

    private int page;

    private String promoter;

    private String processKey;
}
