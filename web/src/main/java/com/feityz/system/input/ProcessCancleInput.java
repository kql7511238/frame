package com.feityz.system.input;

import lombok.Data;

import java.util.List;

@Data
public class ProcessCancleInput {

    public List<String> bizIds;

    private String opinion;

    private String operation;
}
