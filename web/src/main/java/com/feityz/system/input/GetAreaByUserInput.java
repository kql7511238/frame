package com.feityz.system.input;

import lombok.Data;

@Data
public class GetAreaByUserInput {
    private String userNum;

    private String roleName;
}
