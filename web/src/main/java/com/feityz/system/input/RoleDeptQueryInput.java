package com.feityz.system.input;

import lombok.Data;

@Data
public class RoleDeptQueryInput {
    private String dept;

    private String roleName;
}
