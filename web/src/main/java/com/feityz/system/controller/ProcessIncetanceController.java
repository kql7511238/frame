package com.feityz.system.controller;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bstek.uflo.service.HistoryService;
import com.bstek.uflo.service.ProcessService;
import com.feityz.common.Rest;
import com.feityz.system.entity.ProcessIncetanceVo;
import com.feityz.system.entity.User;
import com.feityz.system.input.IncetanceInput;
import com.feityz.system.input.ProcessIncetanceHisInput;
import com.feityz.system.input.ProcessIncetanceInput;
import com.feityz.system.service.IProcessIncetanceService;
import com.feityz.system.service.ITaskInfoService;
import com.feityz.system.service.IUserService;
import com.feityz.system.vo.RollBackInput;
import com.feityz.util.SpringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

@Controller
@Api(tags = "流程实例接口")
@RequestMapping("/processIncetance")
public class ProcessIncetanceController {

    @Autowired
    private IProcessIncetanceService processIncetanceService;
    @Autowired
    private ProcessService processService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private IUserService userService;
    @Autowired
    private ITaskInfoService taskInfoService;

    @ApiOperation(value = "跳转到流程监控页面(处理人)", notes = "跳转到流程监控页面(处理人)")
    @GetMapping("/assignee")
    @ApiIgnore
    public String assignee(){
        return "process/assignee";
    }

    @ApiOperation(value = "跳转到流程监控页面(发起人)", notes = "跳转到流程监控页面(发起人)")
    @GetMapping("/promoter")
    @ApiIgnore
    public String promoter(){
        return "process/promoter";
    }

    @ApiOperation(value = "打开通用查看页面", notes = "打开通用查看页面(发起人)")
    @GetMapping("/commonView")
    @ApiIgnore
    public String commonView(Long instanceId, Model model){

        Map result = processIncetanceService.getInstanceParms(instanceId);
        model.addAttribute("instanceId",instanceId);
        model.addAttribute("url",result.get("url"));
        model.addAttribute("bizId",result.get("bizId"));
        return "process/view";
    }

    @ApiOperation(value = "流程监控(处理人)", notes = "流程监控(处理人)")
    @PostMapping("/assignee/list")
    @ResponseBody
    @ApiIgnore
    public Rest listByAssignee(ProcessIncetanceVo condition, int page, int limit){

        /*if(StringUtils.isEmpty(condition.getAssignee())){
            condition.setAssignee(SpringUtils.getLoginUser().getId().toString());
        }

        IPage<ProcessIncetanceVo> pageInfo =
                processIncetanceService.pageByAssignee(condition,new Page<ProcessIncetanceVo>(page, limit));
        return Rest.success().setData(pageInfo.getRecords()).setTotal(pageInfo.getTotal());*/
        return Rest.success();
    }
    @ApiOperation(value = "流程监控(发起人)", notes = "流程监控(发起人)")
    @PostMapping("/promoter/list")
    @ResponseBody
    public Rest listByPromoter(@RequestBody ProcessIncetanceInput condition){
        if(StringUtils.isEmpty(condition.getPromoter())){
            condition.setPromoter(SpringUtils.getLoginUser().getId().toString());
        }else{
            User user = userService.getUserByCode(condition.getPromoter());
            Assert.notNull(user,"找不到工号为"+condition.getPromoter()+"的人员信息");
            condition.setPromoter(user.getId().toString());
        }
        IPage<ProcessIncetanceVo> pageInfo =
                processIncetanceService.pageByPromoter(condition,new Page<ProcessIncetanceVo>(condition.getPage(), condition.getLimit()));
        return Rest.success().setData(pageInfo.getRecords()).setTotal(pageInfo.getTotal());
    }

    @PostMapping("/promoter/form/list")
    @ResponseBody
    public Rest listByPromoterForm(ProcessIncetanceInput condition){
        return listByPromoter(condition);
    }


    @ApiOperation(value = "历史流程(发起人)", notes = "历史流程(发起人)")
    @PostMapping("/promoter/listHis")
    @ResponseBody
    public Rest pageByPromoterHis(@RequestBody ProcessIncetanceHisInput condition){
        if(StringUtils.isEmpty(condition.getPromoter())){
            condition.setPromoter(SpringUtils.getLoginUser().getId().toString());
        }else{
            User user = userService.getUserByCode(condition.getPromoter());
            Assert.notNull(user,"找不到工号为"+condition.getPromoter()+"的人员信息");
            condition.setPromoter(user.getId().toString());
        }
        IPage<ProcessIncetanceVo> pageInfo =
                processIncetanceService.pageByPromoterHis(condition, new Page<>(condition.getPage(), condition.getLimit()));
        return Rest.success().setData(pageInfo.getRecords()).setTotal(pageInfo.getTotal());
    }

    @ApiOperation(value = "流程监控(所有)", notes = "流程监控(所有)")
    @PostMapping("/all")
    @ResponseBody
    public Rest listAll(@RequestBody IncetanceInput input){
        ProcessIncetanceVo condition = new ProcessIncetanceVo();
        condition.setProcessKey(input.getProcessKey());
        IPage<ProcessIncetanceVo> pageInfo =
                processIncetanceService.pageAll(condition,new Page<ProcessIncetanceVo>(input.getPage(), input.getLimit()));
        return Rest.success().setData(pageInfo.getRecords()).setTotal(pageInfo.getTotal());
    }

    @ApiOperation(value = "结束流程(作废)", notes = "结束流程(作废)")
    @PostMapping("/end")
    @ResponseBody
    public Rest end(@RequestBody RollBackInput input){
        processIncetanceService.endProcessInstance(input);
        return Rest.success();
    }

}
