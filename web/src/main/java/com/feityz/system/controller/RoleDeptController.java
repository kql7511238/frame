package com.feityz.system.controller;

import com.feityz.common.Rest;
import com.feityz.system.entity.Role;
import com.feityz.system.entity.User;
import com.feityz.system.input.RoleAreaInput;
import com.feityz.system.input.RoleDeptInput;
import com.feityz.system.input.RoleDeptQueryInput;
import com.feityz.system.service.IRoleAreaService;
import com.feityz.system.service.IRoleDeptService;
import com.feityz.system.service.IRoleService;
import com.feityz.system.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@Api(tags = "角色部门人员配置")
@Controller
@RequestMapping("/roleDept")
public class RoleDeptController {

    @Autowired
    private IRoleService roleService;
    @Autowired
    private IRoleDeptService roleDeptService;


    @ApiOperation(value = "保存", notes = "保存")
    @ResponseBody
    @PostMapping("/save")
    public Rest save(@RequestBody RoleDeptInput input) {
        roleDeptService.saveRelation(input);
        return Rest.success();
    }
    @ApiOperation(value = "列表", notes = "列表")
    @ResponseBody
    @PostMapping("/list")
    public Rest list(@RequestBody RoleDeptQueryInput input) {

        return Rest.success().setData(roleDeptService.listAll(input));
    }


    @ApiOperation(value = "删除", notes = "删除")
    @ResponseBody
    @GetMapping("/delete")
    public Rest save(Long id) {
        roleDeptService.delete(id);
        return Rest.success();
    }

    @ApiOperation(value = "获取所有角色", notes = "获取所有角色")
    @ResponseBody
    @PostMapping("/listAllRole")
    public Rest listAllRole() {

        List<Role> list = roleService.lambdaQuery()
                .like(Role::getRoleName,"签收")
                .list();

        return Rest.success().setData(list);
    }
}
