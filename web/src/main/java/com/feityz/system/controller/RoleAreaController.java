package com.feityz.system.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.poi.excel.ExcelUtil;
import com.feityz.common.Rest;
import com.feityz.system.entity.Role;
import com.feityz.system.entity.User;
import com.feityz.system.input.GetAreaByUserInput;
import com.feityz.system.input.RoleAreaInput;
import com.feityz.system.input.RoleAreaQueryInput;
import com.feityz.system.service.IRoleAreaService;
import com.feityz.system.service.IRoleService;
import com.feityz.system.service.IUserService;
import com.feityz.system.vo.RoleAreaUserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "角色辖区人员配置")
@Controller
@RequestMapping("/roleArea")
public class RoleAreaController {

    @Autowired
    private IRoleAreaService roleAreaService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IRoleService roleService;
    @ApiIgnore
    @GetMapping("/index")
    private String index(){
        return "roleArea/index";
    }
    @ApiOperation(value = "保存", notes = "保存")
    @ResponseBody
    @PostMapping("/save")
    public Rest save(@RequestBody RoleAreaInput input) {
        roleAreaService.saveRelation(input);
        return Rest.success();
    }

    @ApiOperation(value = "列表", notes = "列表")
    @ResponseBody
    @PostMapping("/list")
    public Rest list(@RequestBody RoleAreaQueryInput input) {

        return Rest.success().setData(roleAreaService.listAll(input));
    }

    @ApiOperation(value = "辖区主管", notes = "辖区主管")
    @ResponseBody
    @PostMapping("/listXqzg")
    public Rest listXqzg(@RequestBody RoleAreaQueryInput input) {
        List<RoleAreaUserVo> roleAreaUserVos = roleAreaService.listAll(input);
        List<RoleAreaUserVo> result = roleAreaUserVos.stream().filter(x -> x.getRoleName().equals("辖区主管")).collect(Collectors.toList());
        return Rest.success().setData(result);
    }

    @ApiOperation(value = "导出", notes = "导出")
    @ResponseBody
    @PostMapping("/export")
    public void export(@RequestBody RoleAreaQueryInput input, HttpServletResponse response) {
        ExportParams params = new ExportParams("角色配置信息", "角色配置信息", ExcelType.XSSF);
        List<RoleAreaUserVo> roleAreaUserVos = roleAreaService.listAll(input);
        Workbook workbook = ExcelExportUtil.exportExcel(params, RoleAreaUserVo.class, roleAreaUserVos);
        try {
            response.addHeader("Content-Disposition", "attachment;filename=角色配置信息.xlsx");
            response.setContentType("application/vnd.ms-excel;charset=gb2312");
            OutputStream output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            workbook.write(bufferedOutPut);
            bufferedOutPut.flush();
            bufferedOutPut.close();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @ApiOperation(value = "所有人员", notes = "所有人员")
    @ResponseBody
    @PostMapping("/listAlluser")
    public Rest listAllUser() {

        List<User> list = userService.lambdaQuery().list();

        return Rest.success().setData(list);
    }

    @ApiOperation(value = "获取所有角色", notes = "获取所有角色")
    @ResponseBody
    @PostMapping("/listAllRole")
    public Rest listAllRole() {

        List<Role> list = roleService.lambdaQuery()
                .list();

        return Rest.success().setData(list);
    }

    @ApiOperation(value = "删除", notes = "删除")
    @ResponseBody
    @GetMapping("/delete")
    public Rest save(Long id) {
        roleAreaService.delete(id);
        return Rest.success();
    }

    @ApiOperation(value = "根据人员票证类型，获取辖区", notes = "删除")
    @ResponseBody
    @PostMapping("/getAreaByUser")
    public Rest getAreaByUser(GetAreaByUserInput input){

       return Rest.success().setData(roleAreaService.getAreaByUser(input));
    }
}
