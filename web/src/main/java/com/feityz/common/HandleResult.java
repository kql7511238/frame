package com.feityz.common;

import lombok.Getter;

@Getter
public enum HandleResult {

    Agree("同意","1"),
    Disagree("不同意","0")
    ;

    private String desc;//文字描述
    private String code; //对应的代码

    HandleResult(String desc, String code) {
        this.desc=desc;
        this.code=code;
    }
}
