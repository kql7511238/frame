package com.bstek.uflo.console.util;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class UserUtil {

    public static String getUserNum(String id) {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("http://localhost:82/Flows/user/get?id="+id);
        HttpResponse response = null;
        try {
            response = client.execute(request);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String strResult = EntityUtils.toString(response.getEntity());
                JSON parse = JSONUtil.parse(strResult);
                JSONObject data = (JSONObject) parse.getByPath("data");
                return data.get("userNum") +"-"+ data.get("userName");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
