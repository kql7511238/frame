package com.bstek.uflo.console.dto;

import com.bstek.uflo.model.task.TaskParticipator;

public class TaskParticipatorDto extends TaskParticipator {

    private String userNum;

    public String getUserNum() {
        return userNum;
    }

    public void setUserNum(String userNum) {
        this.userNum = userNum;
    }
}
